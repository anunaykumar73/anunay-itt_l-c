import pickle
import random

class ITTDB:
	def insertData(self,employee):
		data_to_store = {'Id':random.randint(1,100), 'Name': employee.name, 'Address': employee.add}
		with open('ittdb.txt', 'ab') as file_handle:
			pickle.dump(data_to_store, file_handle, protocol=pickle.HIGHEST_PROTOCOL)
		file_handle.close()

	def printData(self):
		with open('ittdb.txt', 'rb') as file_handle:
			data_to_print = []
			while 1:
    				try:
        				p = pickle.load(file_handle)
    				except EOFError:
        				break
    				data_to_print.append(p)
			for i in range(0, len(data_to_print)):
				print("Id: %s || Name: %s || Address: %s" % (data_to_print[i]["Id"], data_to_print[i]["Name"], data_to_print[i]["Address"]))
		file_handle.close()

	def searchData(self, key):
		with open('ittdb.txt', 'rb') as file_handle:
			data_to_search = []
			while 1:
    				try:
        				p = pickle.load(file_handle)
    				except EOFError:
        				break
    				data_to_search.append(p)
			for i in range(0, len(data_to_search)):
				if data_to_search[i]["Name"] == key:
					print("Found !")
					print("Id: %s || Name: %s || Address: %s" % (data_to_search[i]["Id"], data_to_search[i]["Name"], data_to_search[i]["Address"]))
					break;
