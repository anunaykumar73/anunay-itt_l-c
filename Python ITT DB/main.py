from ittdb import ITTDB
from employee import employee

choice = 1;
print("-----------------Hello User! Enter Choice-------------------")
while choice:
	print("1.Make a new entry")
	print("2.View database content")
	print("3.Search by name")
	print("0.Exit")

	choice = input()
	db = ITTDB()

	if choice==1:
		nameArg = raw_input("Name?:")
		addressArg = raw_input("Address?:")
		emp1 = employee(nameArg,addressArg)
		try:
			db.insertData(emp1)
		except IOError:
			print "Could not write to db"
	if choice==2:
		try:
			db.printData()
		except IOError:
			print "Could not read from db"

	if choice==3:
		key = raw_input("Name?:")
		try:
			db.searchData(key)
		except IOError:
			print "Could not search from db"

 	print("---------------------------X-X-X-X-X-X-X-X----------------------------")
	print("---------------------------X-X-X-X-X-X-X-X----------------------------")
