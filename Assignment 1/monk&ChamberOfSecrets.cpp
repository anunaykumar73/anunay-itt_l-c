#include <bits/stdc++.h>
using namespace std;

int totalSpiders , selectedSpiders, selectedSpiderPower = 0 ;
queue<int> spiderQ ;


int evaluateSpiderMaxPower(int *);


int main()
{
 
    cin>> totalSpiders >> selectedSpiders ;
    int powerArray[totalSpiders+1] ;
    for (int i=1;i<=totalSpiders;i++)
    {
     cin>>powerArray[i] ;
     spiderQ.push(i) ;
    }
	
    evaluateSpiderMaxPower(powerArray);
    
    cout<<endl ;
    return 0;
}



int evaluateSpiderMaxPower(int * powerArray){
    int finalSpiderIndex, index ;
    //maintaining list of spiders evaluated
    bool evaluatedSpiderList[totalSpiders+1] ;
    memset(evaluatedSpiderList,false,sizeof(evaluatedSpiderList)) ;
    int spidersRemaining = totalSpiders ;
     //Starting calculation
    for (int i=0;i<selectedSpiders;i++)
    {
     int limit, temp = 0 ;
     limit = min(selectedSpiders,spidersRemaining) ;
     selectedSpiderPower = -1 ;
     
        while (temp < limit)
            {
                index = spiderQ.front() ;
                if (evaluatedSpiderList[index]) 
                    {
                    spiderQ.pop() ;
                    continue ;
                    }
                    
                //selecting maximum power spider from Q
                if (powerArray[index] > selectedSpiderPower)
                    {
                    selectedSpiderPower = powerArray[index] ;
                    finalSpiderIndex = index ;
                    }
            spiderQ.pop() ;
            spiderQ.push(index) ;
            temp++ ;
  
                if (powerArray[index]) powerArray[index]-=1 ;
            }
			
     evaluatedSpiderList[finalSpiderIndex] = true ;
     cout<<finalSpiderIndex<<" "  ;
     spidersRemaining-- ;
     
    }
              
    return 0;
}